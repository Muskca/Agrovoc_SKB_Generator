/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agrovoc_skb_generator;

import com.fasterxml.jackson.databind.JsonNode;
import fr.irit.sparql.Proxy.SparqlProxy;
import fr.irit.sparql.query.Exceptions.SparqlEndpointUnreachableException;
import fr.irit.sparql.query.Exceptions.SparqlQueryMalFormedException;
import fr.irit.sparql.query.Select.SparqlSelect;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author Fabien
 */
public class Agrovoc_SKB_Generator {

    public static HashMap<String, String> classMappings;
    public static String baseUri = "http://ontology.irstea.fr/agronomictaxon/SKB/Agrovoc#";
    public static String taxonomyUri = baseUri+"Agrovoc_Thesaurus";
    public static SparqlProxy spIn;
    
    public static HashMap<String, String> taxaUris = new HashMap<>();
    
    public static boolean toUpper = true;
    public static StringBuilder out;
    public static int nbTaxonComputed = 0;
    public static int nbUpperTaxonComputed = 0;
    public static int nbNewClass = 0;
    
    public static String cleanString(String s){
        return s.replaceAll(" ", "_").replaceAll("\\.", "");
    }
    
//    public static String generateTriple(String subject, String predicate, String object){
//        String ret = "<"+subject+">";
//        if(predicate.equals("a")){
//            String type = classMappings.get(object);
//            boolean newClass= false;
//            if(type == null){
//                type = baseUri+cleanString(object.toLowerCase().substring(0, object.indexOf(" ")));
//                newClass = true;
//            }
//            ret += " <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <"+type+">.\n";
//            if(newClass){
//                ret += "<"+type+"> <http://www.w3.org/2000/01/rdf-schema#subClassOf> <http://ontology.irstea.fr/AgronomicTaxon#Taxon>.\n";
//            }
//        }
//        else if(predicate.equals("rdfs:label")){
//            ret += " <http://www.w3.org/2000/01/rdf-schema#label> "+object+".\n";
//        }
//        else{
//            ret += " "+predicate+" ";
//            if(object.startsWith("http"))
//                ret += "<"+object+">.\n";
//            else
//                ret += object+". \n";
//        }
//        
//        
//        return ret;
//    }
    
    
    public static String getFragment(String uri){
        if(uri.contains("#"))
            return uri.substring(uri.lastIndexOf("#"));
        else 
            return uri.substring(uri.lastIndexOf("/"));
    }
    
    public static String addNewClass(String classUri){
        String taxonUri = null;
        nbNewClass++;
        
        HashMap<String, String> prefixes = new HashMap<>();
        prefixes.put("skos", "<http://www.w3.org/2004/02/skos/core#>");
        
        ArrayList<String> prefLabels = new ArrayList<>();
        SparqlSelect query = new SparqlSelect(prefixes.entrySet(), "*", "<"+classUri+"> skos:prefLabel ?a.");
        try 
        {       
            JsonNode firstLabel = null;
            for(JsonNode jnode : spIn.getResponse(query))
            {
                JsonNode a = jnode.get("a");
                System.out.println("\t pref label : "+a.get("value").asText());
                JsonNode lang = a.get("xml:lang");
                String rdfsLabelString = "<http://www.w3.org/2000/01/rdf-schema#label> \""+a.get("value").asText()+"\"";
                if(a.get("xml:lang") != null){
                    System.out.println("\t lang : "+lang.asText());
                    if(taxonUri == null && lang.asText().equals("en")){
                        taxonUri = baseUri+cleanString(a.get("value").asText());
                    }
                    rdfsLabelString += "@"+lang.asText()+".\n";
                }
                else{
                    rdfsLabelString += ".\n";
                    if(firstLabel == null){
                        firstLabel = a;
                    }
                }
                prefLabels.add(rdfsLabelString);
            }
            
            
            
            if(taxonUri == null && firstLabel != null){
                taxonUri = baseUri+firstLabel.get("value").asText();
            }
            else{
                taxonUri = baseUri+getFragment(classUri);
            }
            out.append("<"+taxonUri+"> <http://www.w3.org/2000/01/rdf-schema#subClassOf> <http://ontology.irstea.fr/agronomictaxon/core#Taxon>.\n");
            out.append("<"+taxonUri+"> <http://www.w3.org/2000/01/rdf-schema#seeAlso> <"+classUri+">.\n");
            for(String prefLabel : prefLabels){
                out.append("<"+taxonUri+"> "+prefLabel);
            }
            
            query = new SparqlSelect(prefixes.entrySet(),"*", "<"+classUri+"> skos:altLabel ?a.");
            for(JsonNode jnode : spIn.getResponse(query))
            {
                JsonNode a = jnode.get("a");
                System.out.println("\t alt label : "+a.get("value").asText());
                String rdfsLabelString = "<"+taxonUri+"> <http://www.w3.org/2000/01/rdf-schema#label> \""+a.get("value").asText()+"\"";
                JsonNode lang = a.get("xml:lang");
                if(a.get("xml:lang") != null){
                    System.out.println("\t lang : "+lang.asText());
                    rdfsLabelString += "@"+lang.asText()+".\n";
                }
                else {
                    rdfsLabelString += ".\n";
                }
                out.append(rdfsLabelString);
            }
        } 
        catch (SparqlQueryMalFormedException ex) 
        {
            System.err.println("Query mal formed ...");
        } 
        catch (SparqlEndpointUnreachableException ex) 
        {
            System.err.println("Sparql endpoint unreachable ...");
        }
        return taxonUri;
    }
    
    
    
    public static ArrayList<String> concatTaxon(String uri){
        ArrayList<String> ret = new ArrayList<>();
        String upperTaxonUri = null;
        ArrayList<String> narrowerUris = new ArrayList<>();
        
        System.out.println("Compute : "+uri);
        String taxonUri = null;
        
        HashMap<String, String> prefixes = new HashMap<>();
        prefixes.put("skos", "<http://www.w3.org/2004/02/skos/core#>");
        
        ArrayList<String> prefLabels = new ArrayList<>();
        SparqlSelect query = new SparqlSelect(prefixes.entrySet(), "*", "<"+uri+"> skos:prefLabel ?a.");
        try 
        {       
            JsonNode firstLabel = null;
            for(JsonNode jnode : spIn.getResponse(query))
            {
                JsonNode a = jnode.get("a");
                System.out.println("\t pref label : "+a.get("value").asText());
                JsonNode lang = a.get("xml:lang");
                String rdfsLabelString = "<http://www.w3.org/2000/01/rdf-schema#label> \""+a.get("value").asText()+"\"";
                String scientificString = "<http://ontology.irstea.fr/AgronomicTaxon#hasScientificName> \""+a.get("value").asText()+"\"";
                if(a.get("xml:lang") != null){
                    System.out.println("\t lang : "+lang.asText());
                    if(taxonUri == null && lang.asText().equals("en")){
                        taxonUri = baseUri+cleanString(a.get("value").asText());
                    }
                    rdfsLabelString += "@"+lang.asText()+".\n";
                    scientificString += "@"+lang.asText()+".\n";
                }
                else{
                    rdfsLabelString += ".\n";
                    scientificString += ".\n";
                    if(firstLabel == null){
                        firstLabel = a;
                    }
                }
                prefLabels.add(rdfsLabelString);
                prefLabels.add(scientificString);
            }
            
            query = new SparqlSelect(prefixes.entrySet(), "*", "<"+uri+"> <http://aims.fao.org/aos/agrontology#hasTaxonomicRank> ?a.");
            for(JsonNode jnode : spIn.getResponse(query))
            {
                String taxLevel = jnode.get("a").get("value").asText();
                System.out.println("\t taxonomic level : "+taxLevel);
                if(classMappings.containsKey(taxLevel)){
                    out.append("<"+taxonUri+"> a <"+classMappings.get(taxLevel)+">.\n");
                }
                else{
                    String classUri = addNewClass(taxLevel);
                    if(classUri != null){
                        classMappings.put(taxLevel, classUri);
                        out.append("<"+taxonUri+"> a <"+classUri+">.\n");
                    }
                }
            }
            
            System.out.println("TEST : "+taxonUri+" -- "+firstLabel);
            if(taxonUri == null && firstLabel != null){
                taxonUri = baseUri+firstLabel.get("value").asText();
                System.out.println("ADD TAXA URI : "+uri+" -- "+taxonUri);
                
            }
            taxaUris.put(uri, taxonUri);
            out.append("<"+taxonomyUri+"> <http://ontology.irstea.fr/AgronomicTaxon#memberScheme> <"+taxonUri+">.\n");
            out.append("<"+taxonUri+"> <http://www.w3.org/2004/02/skos/core#inScheme> <"+taxonomyUri+">.\n");
            out.append("<"+taxonUri+"> <http://www.w3.org/2000/01/rdf-schema#seeAlso> <"+uri+">.\n");
            for(String prefLabel : prefLabels){
                out.append("<"+taxonUri+"> "+prefLabel);
            }
            
            query = new SparqlSelect(prefixes.entrySet(),"*", "<"+uri+"> skos:altLabel ?a.");
            for(JsonNode jnode : spIn.getResponse(query))
            {
                JsonNode a = jnode.get("a");
                System.out.println("\t alt label : "+a.get("value").asText());
                String rdfsLabelString = "<"+taxonUri+"> <http://www.w3.org/2000/01/rdf-schema#label> \""+a.get("value").asText()+"\"";
                String vernacularString = "<"+taxonUri+"> <http://ontology.irstea.fr/agronomictaxon/core#hasVernacularName> \""+a.get("value").asText()+"\"";
                JsonNode lang = a.get("xml:lang");
                if(a.get("xml:lang") != null){
                    System.out.println("\t lang : "+lang.asText());
                    rdfsLabelString += "@"+lang.asText()+".\n";
                    vernacularString += "@"+lang.asText()+".\n";
                }
                else {
                    rdfsLabelString += ".\n";
                    vernacularString += ".\n";
                }
                out.append(rdfsLabelString);
                out.append(vernacularString);
            }
            
            query = new SparqlSelect(prefixes.entrySet(),"DISTINCT ?a", "<"+uri+"> skos:broader ?a.");
            //System.out.println(query);
            for(JsonNode jnode : spIn.getResponse(query))
            {
                JsonNode a = jnode.get("a").get("value");
                upperTaxonUri = a.asText();
                
                String eqUpperTaxonUri = taxaUris.get(upperTaxonUri);
                System.out.println("\t upper Taxon : "+upperTaxonUri+" ("+eqUpperTaxonUri+")");
                if(eqUpperTaxonUri != null){
                    String upperTaxonTriple = "<"+taxonUri+"> <http://ontology.irstea.fr/agronomictaxon/core#hasHigherRank> <"+eqUpperTaxonUri+">.";
                    out.append(upperTaxonTriple);
                }
            }
            
            query = new SparqlSelect(prefixes.entrySet(),"*", "<"+uri+"> skos:broader ?a.");
            //System.out.println(query);
            for(JsonNode jnode : spIn.getResponse(query))
            {
                JsonNode a = jnode.get("a").get("value");
                upperTaxonUri = a.asText();
                
                String eqUpperTaxonUri = taxaUris.get(upperTaxonUri);
                System.out.println("\t upper Taxon : "+upperTaxonUri+" ("+eqUpperTaxonUri+")");
                if(eqUpperTaxonUri != null){
                    String upperTaxonTriple = "<"+taxonUri+"> <http://ontology.irstea.fr/agronomictaxon/core#hasHigherRank> <"+eqUpperTaxonUri+">.";
                    out.append(upperTaxonTriple);
                }
            }
            
            query = new SparqlSelect(prefixes.entrySet(),"*", "<"+uri+"> skos:narrower ?a.");
            for(JsonNode jnode : spIn.getResponse(query))
            {
                JsonNode a = jnode.get("a").get("value");
                String narrowerUri = a.asText();
                System.out.println("\t narrower Taxon : "+narrowerUri);
                narrowerUris.add(narrowerUri);
            }
            
        } 
        catch (SparqlQueryMalFormedException ex) 
        {
            System.err.println("Query mal formed ...");
        } 
        catch (SparqlEndpointUnreachableException ex) 
        {
            System.err.println("Sparql endpoint unreachable ...");
        }

        if(uri.compareToIgnoreCase("http://aims.fao.org/aos/agrovoc/c_7950") == 0){
            concatTaxon(upperTaxonUri);
            toUpper = false;
            for(String nUri :narrowerUris){
                ret.add(nUri);
            }
        }
        else{
            if(toUpper && upperTaxonUri != null 
                    && !uri.equalsIgnoreCase("http://aims.fao.org/aos/agrovoc/c_330074")){ // don't go upper than Plantae
                nbUpperTaxonComputed++;
                concatTaxon(upperTaxonUri);
                String eqUpperTaxonUri = taxaUris.get(upperTaxonUri);
                System.out.println("\t upper Taxon : "+upperTaxonUri+" ("+eqUpperTaxonUri+")");
                if(eqUpperTaxonUri != null){
                    String upperTaxonTriple = "<"+taxonUri+"> <http://ontology.irstea.fr/agronomictaxon/core#hasHigherRank> <"+eqUpperTaxonUri+">.";
                    out.append(upperTaxonTriple);
                }
            }
            else{
                for(String nUri: narrowerUris){
                    ret.add(nUri);
                }
            }
        }
        nbTaxonComputed ++;
        return ret;
    }
    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        spIn = SparqlProxy.getSparqlProxy("http://localhost:3030/Agrovoc/");
       
        
       classMappings = new HashMap<>();
       classMappings.put("http://aims.fao.org/aos/agrovoc/c_11125", "http://ontology.irstea.fr/agronomictaxon/core#GenusRank");
       classMappings.put("http://aims.fao.org/aos/agrovoc/c_331243", "http://ontology.irstea.fr/agronomictaxon/core#SpecyRank");
       classMappings.put("http://aims.fao.org/aos/agrovoc/c_330936", "http://ontology.irstea.fr/agronomictaxon/core#OrderRank");
       classMappings.put("http://aims.fao.org/aos/agrovoc/c_330935", "http://ontology.irstea.fr/agronomictaxon/core#ClassRank");
       classMappings.put("http://aims.fao.org/aos/agrovoc/c_330949", "http://ontology.irstea.fr/agronomictaxon/core#PhylumRank");
       classMappings.put("http://aims.fao.org/aos/agrovoc/c_330934", "http://ontology.irstea.fr/agronomictaxon/core#KingdomRank");
//       //classMappings.put("Subspecies Subspecies", baseUri+"SUBSPECIES/");
        
        out = new StringBuilder();
        System.out.println("Export ontological module prefixes ... ");
        try {
            String modulePrefixes = FileUtils.readFileToString(new File("in/agronomicTaxon_prefix.ttl"), "utf-8");
            out.append(modulePrefixes);
        } catch (IOException ex) {
            System.err.println("Error during prefixes export : ");
            System.err.println(ex);
        }
        System.out.println("Prefixes exported");
        
        System.out.println("Export ontological module ... ");
        try {
            String moduleTriples = FileUtils.readFileToString(new File("in/agronomicTaxon_2.ttl"), "utf-8");
            out.append(moduleTriples);
        } catch (IOException ex) {
            System.err.println("Error during prefixes export : ");
            System.err.println(ex);
        }
        System.out.println("Module exported");
       out.append("<"+baseUri+"> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#Ontology>.\n");
       
       out.append("<"+taxonomyUri+"> a <http://ontology.irstea.fr/agronomictaxon/core#Taxonomy>.\n");
       ArrayList<String> narUris = concatTaxon("http://aims.fao.org/aos/agrovoc/c_7950");
       while(!narUris.isEmpty()){
           String narUri = narUris.remove(0); // pop first
           narUris.addAll(concatTaxon(narUri));
       }
       System.out.println(out);
       System.out.println(nbTaxonComputed+" Taxa computed ("+nbUpperTaxonComputed+" upper taxon)");
       System.out.println("NEW CLASS : "+nbNewClass);
        try {
            FileUtils.write(new File("out_Agrovoc.ttl"), out);
        } catch (IOException ex) {
            System.err.println("FILE WRITTER ERROR");
        }
    }
    
}
